clear all;

%rng(714658427);
%rng(714707728);
rng('shuffle');
%rng(714413967);                         % node 2 is pure orig while
%                                        % node 3 is pure dest 
num_node = 500;
cost = randi(1000, num_node,num_node);
cost(cost>250)=Inf;
cost(sub2ind(size(cost), 1:num_node, 1:num_node)) = 0;
tic
[min_cost next_node path] = Floyd_Warshall(cost);
toc
max_length = 0;
max_pair = [];
for orig=1:num_node
    for dest=1:num_node
        if length(path{orig,dest}) > max_length
            max_length = length(path{orig,dest});
            max_pair = [orig dest];
        end
    end
end

% Dijkstra
% $$$ tic
% $$$ for orig=1:num_node
% $$$     for dest=1:num_node
% $$$         farthestPreviousHop = 1:num_node;
% $$$         farthestNextHop = 1:num_node;
% $$$         [path{orig,dest}, min_cost2(orig,dest), ...
% $$$          farthestPreviousHop, farthestNextHop] = ...
% $$$             dijkstra(num_node, ...
% $$$                      cost, orig, dest, ...
% $$$                      farthestPreviousHop, farthestNextHop);
% $$$     end
% $$$ end
% $$$ toc
% 
orig=max_pair(1)
dest=max_pair(2)
sp_orig = path{orig,dest}(1:(end-1));
sp_dest = path{orig,dest}(2:end);
C=[cost(sub2ind(size(cost),sp_orig,sp_dest))];
[sp_orig' sp_dest' C']
[sum(C) min_cost(orig,dest)]

