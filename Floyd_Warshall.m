function [min_cost, next_node, path] = Floyd_Warshall(cost)
% FLOYD_WARSHALL - Find the shortest path for each of all node pairs
%   This is inspired by 'Fast Floyd' by Dustin Arendt
%   (http://www.mathworks.com/matlabcentral/fileexchange/25776-vectorized-floyd-warshall).
%   ----------------------------------------
%   usage
%   ----------------------------------------
%   min_cost = Floyd_Warshall(cost);
%   [min_cost, next_node] = Floyd_Warshall(cost);
%   [min_cost, next_node, path] = Floyd_Warshall(cost);
%   ---------------------------------------- 
%   input
%   ---------------------------------------- 
%   - cost: the edge weight matrix, which is a regular matrix whose
%   dimension equivalent to the number of node. cost(i, j) is the
%   edge weight from node i to node j. When i and j are not
%   connected then cost(i, j) should be Inf.
%   
%   ----------------------------------------
%   output
%   ----------------------------------------
%   - min_cost: minimum costs matrix. min_cost(i, j) is the
%   minimum cost from node i to node j.
%   - next_node: next nodes matrix. next_node(i, j) is the next
%   node from node i on the shortest path to node j.
%   - path: nodes on the shotest paths. path{i, j} is the ordered
%   node list on the shortest path from node i to j.
%   
%   ----------------------------------------
%   note
%   ----------------------------------------
%   The list that constructs the shortest path from node i to node
%   j can be obtained by
%   > [path{i,j}(1:(end-1)) path{i,j}(2:end)]

% initialize
num_node = size(cost, 1);
min_cost = cost;
next_node = repmat(1:num_node,num_node,1);

% iteration
for k = 1:num_node
    cost_i2k = repmat(min_cost(:, k), 1, num_node);
    cost_k2j = repmat(min_cost(k, :), num_node, 1);
    next_node_i2k = repmat(next_node(:,k),1,num_node);

    % update next node on the shortest path
    is_updated = (min_cost > cost_i2k+cost_k2j);
    next_node(is_updated) = next_node_i2k(is_updated);
    
    % update minimum cost
    min_cost = min(min_cost, cost_i2k+cost_k2j);
end

% construct shortest path if requested
if nargout > 2
    path = cell(num_node,num_node);
    for orig=1:num_node
        for dest=1:num_node
            path{orig,dest}=[];
            if ~isinf(min_cost(orig,dest))
                % add the current node and move to the next node
                % until reach the destinaton
                tmp_path = orig;
                while tmp_path(end)~=dest
                    tmp_path(end+1) = next_node(tmp_path(end),dest);
                end
                path{orig,dest} = tmp_path;
            end
        end
    end
end


